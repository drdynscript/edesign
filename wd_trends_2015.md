Webdesign trends 2015
=====================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Olivier Parent|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

##Trends

* Grotere focus op Mobile. Synergie tussen  Mobile en Desktop
* Eerst digitale huisstijlen dan geprinte
* Geëvolueerde responsive design en assets
* __Design__
	* Enorme (hero) achtergrondafbeeldingen- en video's
	* Split screens
	* Kaart- en tegel gebaseerde ontwerpen
	* Spookknoppen
	* Flat design wordt volwassen
	* Material design (Vibrant design)
	* Dominantie van één kleur
	* Meer nadruk op rijke Typography
	* Opmars van Illustraties en SVG
* __User Experience__
	* Micro UX
	* Unieke website navigatie
	* Meer afbeeldingen en video's, minder tekst
	* Persoonlijk Brand
	* Product eerst
	* Subtiele parallax one page websites
	* Meer scrollen, minder klikken
	* Interactive storytelling
* __Development__
	* Open data, Big Data en Grafieken
	* Isomorphic JavaScript
	* Toename van Web API's
	* Web componenten in combinatie met adaptive design

##Enorme achtergrondafbeeldingen- en video's

![www.airbnb.be](https://lh4.googleusercontent.com/-dsxbP7yMerg/VPS8XeSWkWI/AAAAAAAABR4/pCNxeQiyHgY/s0/fullbkgnd1.PNG "fullbkgnd1.PNG")

![y.co](https://lh3.googleusercontent.com/-a0sSpSF1Sis/VPS9tKCm8FI/AAAAAAAABSI/Ys-PRzaoipc/s0/fullbkgnd2.PNG "fullbkgnd2.PNG")

![mediaboom.com](https://lh3.googleusercontent.com/-91P4WC5Efi8/VPS-ftI0S7I/AAAAAAAABSU/7oj8e1trTzc/s0/fullbkgnd3.PNG "fullbkgnd3.PNG")

Voorbeelden:

* [airbnb.be](https://www.airbnb.be/?locale=nl)
* [y.co](http://y.co/)
* [mediaboom.com](http://mediaboom.com/home)

##Spookknoppen

![Ghost Buttons](https://lh6.googleusercontent.com/-TnSNJgTgfoU/VPRBVo87gUI/AAAAAAAABQo/wF98l-QXW7Q/s0/ghostbtn1.PNG "ghostbtn1.PNG")

![couple.me](https://lh6.googleusercontent.com/-ND4oGKruAic/VPRCAugrhfI/AAAAAAAABQ0/qXvYRtlIzTs/s0/ghostbtn2.PNG "ghostbtn2.PNG")

![parall.ax](https://lh6.googleusercontent.com/-H8qrcTvIxLI/VPREGZrAeKI/AAAAAAAABRI/weQB3_GorPA/s0/ghostbtn3.PNG "ghostbtn3.PNG")

Voorbeelden:

* [iuvo.si](http://www.iuvo.si/)
* [couple.me](https://couple.me/alice)
* [creativeadawards.com](http://www.creativeadawards.com/)
* [gridstylesheets.org](http://gridstylesheets.org/)
* [triplagent.com](http://www.triplagent.com/)
* [hikecon.com](http://hikecon.com/)
* [bilderphoto.com](http://bilderphoto.com/)
* [parall.ax](https://parall.ax/)

##Parallax en One Page Applications

![https://ihatetomatoes.net/merry-christmallax/](https://lh6.googleusercontent.com/-582fGfUHVtU/VPW5gFPQEvI/AAAAAAAABS4/ZD87UfUYfu0/s0/opa1.PNG "https://ihatetomatoes.net/merry-christmallax/")

> **Parallax Bibliotheken**
>  
* [Stellar.js](http://markdalgleish.com/projects/stellar.js/)
* [Jarallax](http://www.jarallax.com/)
* [Skrollr](https://github.com/Prinzhorn/skrollr)
* [parallax.js](http://matthew.wagerfield.com/parallax/)
>
> **Reveal Bibliotheken**
>
> * [wow.js](http://mynameismatthieu.com/WOW/)
> * [scrollReveal.js](http://scrollrevealjs.org/)
> * [SuperScrollOrama](http://johnpolacek.github.io/superscrollorama/)

##Interactive storytelling

* [Echoes of Tsunami](http://www.echoesoftsunami.com/ "Echoes of Tsunami")
* [Firestorm](http://www.theguardian.com/world/interactive/2013/may/26/firestorm-bushfire-dunalley-holmes-family "Firestorm - theguardian")
* [Make your money matter](http://makeyourmoneymatter.org/)
* [MailChimp By Numbers](http://mailchimp.com/2013/#by-the-numbers)
* [NeoMan Studios](http://snip.ly/D7e9#http://neomam.com/interactive/13reasons/)
* [Mac Pro](http://snip.ly/yMIW#http://www.apple.com/mac-pro/)

> Webdesign trends 2015
>   
* http://www.creativebloq.com/web-design/hottest-trends-2015-11513980
* http://www.magenta.be/top-5-webdesign-trends-2015/
* http://www.contentcrackers.be/blog/content-crackers-goes-fortune-telling-vijf-webdesign-trends-voor-2015
* http://thenextweb.com/dd/2015/01/02/10-web-design-trends-can-expect-see-2015/
* http://thenextweb.com/dd/2013/12/29/10-web-design-trends-can-expect-see-2014/2/
* http://www.elegantthemes.com/blog/resources/web-design-trends-to-look-out-for-in-2015
* http://mediaweb.nl/blog/15-webdesign-trends-2015/
* http://www.webdesignerdepot.com/2015/01/4-essential-layout-trends-for-2015/
* https://econsultancy.com/blog/65898-17-crucial-web-design-trends-for-2015/
* http://www.webdesignerdepot.com/2015/01/whats-new-for-designers-january-2015/
