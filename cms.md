Content Management Systems
=========================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Olivier Parent|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

##PHP/MySQL Hosting

Hosting Provider: [ByetHost](https://byethost.com/)

> **OPMERKING:**
>
> Op de mediacampus kunnen we deze registratie niet allemaal doorvoeren omdat we hetzelfde IP adres gebruiken. Voer
deze installatie gewoon uit op een thuislocatie.

---

> **1. Registratie**
> Vul al de noodzakelijke gegevens in. De security code is ook gekend als CAPTCHA. Het eerste veld heeft niks te
maken met het subdomein die we willen aanmaken voor Opdracht 1. Dit veld bevat gewoon jouw nicknaam, naam of iets
anders. Het bevat na registratie de werkruimte waarbinnen je de subdomeinen kan aanmaken.
>
![Registratie](https://lh6.googleusercontent.com/-2oUi-qqVAes/VO2D5l6uCAI/AAAAAAAABJQ/HJRUMuFGwQg/s0/byethost1.png "byethost1.png")

---

> **2. Voltooien van registratie**
> Klik op de link in de ontvangen email. Hierdoor wordt jouw account geactiveerd.
>
> ![Email na registratie](https://lh3.googleusercontent.com/-7Q9g4kpRrFI/VO2E4DrJYYI/AAAAAAAABJo/iB9huVFaQ9U/s0/byethost3.png "byethost3.png")
>
> ![Website Activatie](https://lh5.googleusercontent.com/-RXy0usgkzgs/VO2EkzfAwiI/AAAAAAAABJc/34fnOwEhGyU/s0/byethost2.png "byethost2.png")

---

> **3. Dashboard Hosting Provider**
> Beheer van de hosting.
>
> ![Dashboard Hosting](https://lh5.googleusercontent.com/-AMvXa239RcU/VO2HBRL6uuI/AAAAAAAABKA/tt2Oy-JOKOc/s0/byethost4.png "byethost4.png")

---

> **4. Connectie informatie**
> Instellen van FTP (FileZilla) om bestanden over te zetten. Dit zijn de instellingen van drdynscript (Philippe De
Pauw - Waterschoot). Bij jullie zijn dat andere instellingen die vermeld zijn in de ontvangen emails!
>
> * Cpanel Username:        b22_15905886
* Cpanel Password:        ++++++++++++
* Your URL:               http://drdynscript.byethost22.com or http://www.drdynscript.byethost22.com
* FTP Server :            ftp.byethost22.com
* FTP Login :             b22_15905886
* FTP Password :          ++++++++++++
* MySQL Database Name:    MUST CREATE IN CPANEL
* MySQL Username :        b22_15905886
* MySQL Password :        ++++++++++++
* MySQL Server:           SEE THE CPANEL

---

> **5. FTP Instellingen**
> Instellen van FTP (FileZilla) om bestanden over te zetten.
>
> ![Instellen FTP](https://lh4.googleusercontent.com/-CTva3-alRmY/VO2Hae8BE2I/AAAAAAAABKM/7haVC17cFWY/s0/byethost5.png "byethost5.png")

---

> **6. Subdomein aanmaken**
> Aanmaak van subdomeins in byethost. Maak een subdomein **{Arteveldehogeschool loginnaam}blog** aan. Bv.: `phildpblog` resulterend in de URL: `http://phildpblog.byethost22.com`. In dit nieuw subdomein zullen we het Wordpress CMS installeren via FTP.
>
>![Subdomein aanmaken](https://lh3.googleusercontent.com/-o9GNpnr9hW4/VO2Nwf3szlI/AAAAAAAABK0/7TWWmcvH-uQ/s0/byethost6.png "byethost6.png")

---

> **7. Subdomein via FTP**
> Het aangemaakt subdomein, in dit geval `edesign`, is zichtbaar als folder `edesign.byethost22.com` via FTP. Binnen deze folder zullen we de bestanden van het Wordpress CMS toevoegen.
>
>![Subdomein via FTP](https://lh4.googleusercontent.com/-vkSdABfwWRU/VO2PGMcq57I/AAAAAAAABLE/3-6eU1sMm98/s0/byethost7.png "byethost7.png")


##Wordpress

###Installatie

> **1. Download**
> Download Wordpress 4.1.1 . Na download pak je alle bestanden uit (decomprimeren). [wordpress.org](https://nl.wordpress.org/wordpress-4.1.1-nl_NL.zip)
>
>![Wordpress.org](https://lh4.googleusercontent.com/-_U2bNspgySo/VO2Z8s_c43I/AAAAAAAABLw/A5r0w9woYOA/s0/wordpress1.png "wordpress1.png")
>
>![Uitpakken van archive bestand](https://lh5.googleusercontent.com/-pvFsPpxzm50/VO2anx6E78I/AAAAAAAABL8/XO8J4Kh1THA/s0/wordpress2.png "wordpress2.png")

---

> **2. Overzetten van uitgepakte bestanden**
> We zetten de uitgepakte bestanden van het wordpress CMS over via FTP binnen de reeds aangemaakt subdomein.

---

> **3. Navigeren naar het subdomein**
> We navigeren naar het subdomein in dit geval naar `http://edesign.byethost22.com`. De setup van Wordpress eist vervolgens de aanmaakt van een MySQL databank. Deze databank maken we aan via het controlepaneel van Byethosting.
>
> ![Navigeren naar het subdomein](https://lh3.googleusercontent.com/-ebaxaLF_44I/VO2bJzhmtCI/AAAAAAAABMI/J5RB-Pukor0/s0/wordpress3.png "wordpress3.png")

---

> **4. Aanmaak MySQL databank**
> De MySQL databank wordt aangemaakt via het controlepaneel van Byethosting. Naamgeving databank: **{Arteveldehogeschool loginnaam}blog**. De uiteindelijk naam van deze database bestaat uit `b22_{MySQL username}_{Arteveldehogeschool loginnaam}blog`, bv.: `b22_15905886_phildpblog` of in dit geval  `b22_15905886_edesign`. MySQL databasenaam, gebruikersnaam, paswoord en server hostnaam.
>
> ![Aanmaak MySQL databank](https://lh6.googleusercontent.com/-IaGFyWmljQ4/VO2d0fcvCBI/AAAAAAAABMg/JK1W83dP-JQ/s0/byethost8.png "byethost8.png")

---

> **5. Instelling in `wp-config.php`**
> Het voorbeeldbestand `wp-config-sample.php` bevat de meest toegepaste instellingen in het Wordpress CMS. Allereerst moeten we de naam van dit bestand wijzigen in `wp-config.php`. Vervolgens stellen we de voorgaande MySQL instellingen in. Vergeet ook niet dit bestanden via FTP te verzenden on der root van de reeds aangemaakt subdomein.
>
> ![MySQL instellingen in wp-config.php](https://lh6.googleusercontent.com/-IQVt584cCnc/VO2izMMUmwI/AAAAAAAABNA/q4dHYeS-Znc/s0/wordpress4.png "wordpress4.png")

##Bibliografie

> **PHP**
>
> * php.net/manual/en/
* dev.mysql.com/doc/refman/5.5/en/
>
> **Wordpress**
>
> * codex.wordpress.org
* codex.wordpress.org/WordPress_Lessons
* code.tutsplus.com/categories/wordpress