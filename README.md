E-design
=====================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Stijn Anseel|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

## Lesinhouden

|Lesweek|Omschrijving|
|----|----------|
|1|ECTS, Grid in Photoshop, Bootstrap (1), Nuts and Bolts (1), Herhalingsoefeningen|
|2|Opleidingsvergadering: geen les! Maak de herhalingsoefeningen en de app: CMO showcase!| 
|3|Bootstrap (2), Trends 2015, Full background images, Ghost buttons, JavaScript, jQuery, Briefing Opdracht 1: Blog m
.b.v. WordPress CMS, JavaScript (1)|
|4|CSS3 transformaties, transities en animaties|
|5|Briefing Opdracht 2, Werkweek|
|6|Werkweek|
|7|JavaScript (2)|
|8|Email Campagne, Briefing Opdracht 3|
|-|Paasvakantie|
|9|CSS preprocessors, Automation|
|10|Werkweek|
|11|Werkweek|
|12|Werkweek|
|13|Werkweek, Deadline Opdrachten|

## Cursus

* [Trends in Webdesign & development](https://bitbucket.org/drdynscript/edesign/src/d174dcf701611c689deeee4bbcef3da58f39402c/wd_trends_2015.md?at=master)
* [Content Management Systemen](https://bitbucket.org/drdynscript/edesign/src/d174dcf701611c689deeee4bbcef3da58f39402c/cms.md?at=master)
* [JavaScript Introductie](https://bitbucket.org/drdynscript/edesign/src/d174dcf701611c689deeee4bbcef3da58f39402c/js_introduction.md?at=master)
* [CSS Transformaties, Transities en Animaties](https://bitbucket.org/drdynscript/edesign/src/e9be7bbfa1ef3a9a69fec9c457d1e014f15eb94d/css_transformaties_en_animaties.md?at=master)
* [Les E-design Trends](https://bitbucket.org/drdynscript/edesign/src/4b61b050a8b4c3253372921a423a10aa64856a86
/doc_les2.pdf?at=master)
* [Les Icons](https://bitbucket.org/drdynscript/edesign/src/4b61b050a8b4c3253372921a423a10aa64856a86/ico_les.pdf?at=master)

##Opdrachten

* [Opdrachten](https://bitbucket.org/drdynscript/edesign/src/890f5da18119969b640dbb17d081c52ec9dd2070/opdrachten.md?at=master)

## Voorbeelden

* [JavaScript/jQuery DOM voorbeelden](https://bitbucket
.org/drdynscript/edesign/src/c338b826f0fb5e43581ac83a7be57b681f9a5a57/js_dom_voorbeelden.md?at=master)
* [CSS Transformaties, Transities en Animaties](https://bitbucket.org/drdynscript/edesign/src/4b61b050a8b4c3253372921a423a10aa64856a86/css_animations/?at=master)

## Applicaties

* [Herhalingsoefeningen Webdesign II & III](https://bitbucket.org/drdynscript/edesign/src/d174dcf701611c689deeee4bbcef3da58f39402c/oefeningen/?at=master)
* [Bootstrap Grid e.d.](https://bitbucket.org/drdynscript/edesign/src/d174dcf701611c689deeee4bbcef3da58f39402c/bootstrap_grid/?at=master)
* [GDM of CMS Showcase](https://bitbucket.org/drdynscript/edesign/src/d174dcf701611c689deeee4bbcef3da58f39402c/app_gdm_showcase/?at=master)
* [One Page Application Setup](https://bitbucket.org/drdynscript/edesign/src/d174dcf701611c689deeee4bbcef3da58f39402c/app_opa/?at=master)
