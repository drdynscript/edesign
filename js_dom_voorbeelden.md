JavaScript DOM Voorbeelden
========================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Stijn Anseel|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

##Prelude

* [JavaScript Introductie](https://bitbucket.org/drdynscript/edesign/src/76fb285e47dbb20e1d8eb1c78af28bc109da6219/js_introduction.md?at=master)
* [Code Academy](http://www.codecademy.com/)

##Personal Information

**JavaScript**

```
var firstName = 'Philippe';
var lastName = 'De Pauw - Waterschoot';
var age = 23;
var length = 1.72;
var isFemale = false;
var result = '';
result += 'My Name is ' + firstName + ' ' + lastName + '.';
result += '\r\n' + 'I\'m' + age + ' years old.';
result += '\r\n' + 'My length is ' + length + ' meter.';
result += '\r\n' + 'Oh I forgot! I\'m a ';
if(isFemale){
    result += ' female.';
} else {
    result += ' male.';
}
alert(result);
```

* `var` keyword betekent een variabele.
*  `firstName` is de naam van de variabele.
* De `age` en `length` variabelen bevatten een `Number` datatype. De mogelijke waarden zijn positieve of negatieve gehele en floating point (comma) getallen.
* Een string wordt gedefinieerd tussen enkele aanhalingstekens `'...'` (bij voorkeur) of dubbele aanhalingstekens `"..."`.
* Via `=` kennen we een waarde toe aan deze variabele.
* `alert(...)` is een ingebouwde methode binnen een browser waarmee we berichten kunnen weergeven in een popup venster.
* `console.log(...)` is een ingebouwde methode binnen een browser waarmee we berichten kunnen weergeven in het console-window van deze browser.
* `\r\n` is een **carriage return** enkel toepasbaar in een alert-popup of de console!
* `+=` betekent dat we aan de oorspronkelijke inhoud van een variabele extra inhoud toevoegen.
* `isFemale` is de naam van de variabele die als waarde een Boolean bevat. Een Boolean bevat twee waarden namelijk `true` of `false`.

**Resultaat**

![Persinal Information via JavaScript](https://lh6.googleusercontent.com/-GdIKoK7DKAk/VO4p7W6K1eI/AAAAAAAABO4/s217yn4ATv4/s0/js_personalinformation.PNG "js_personalinformation.PNG")

##Manipulatie van de DOM

**HTML**

```
<div id="person">Philippe De Pauw - Waterschoot</div>
```

**jQuery**

```
var person = $('#person');
var str = $(person).html();
alert(str);
$(person).html('Linus Torvalds');
```
* Om het object op te vragen van het `<div>` element met `id="person"` kunnen we realiseren via `$('#person')`, tussen `(` en `)` vermelden we de waarde van de id voorafgegaan door een hekje `#` of kardinaal-teken. Deze werkwijze is gelijkaardig met het aanspreken van een html element via CSS.
* De inhoud van het bovenstaande `<div>` element kunnen we opvragen via een beschikbare `html()` methode. In jQuery moeten we het `person` object omsluiten door `$(object)` zodat we vervolgens de beschikbare eigenschappen en methode kunnen aanspreken.
* Om de inhoud te veranderen vermelden we een string in diezelfde methode.

![DOM Manipulatie](https://lh5.googleusercontent.com/-l3bq9qk7AfA/VO7jyMXa0gI/AAAAAAAABPQ/rzIa5tDAcmM/s0/js_dommanipulation.PNG "js_dommanipulation.PNG")

##Quote from Albert Einstein

**HTML**

```
<blockquote cite="http://www.brainyquote.com/quotes/quotes/a/alberteins125368.html">Learn from yesterday, live for today, hope for tomorrow. The important thing is not to stop questioning.
    <footer>
        - Geciteerd door <a title="Quotes from Albert Einstein" href="http://www.brainyquote.com/quotes/quotes/a/alberteins125368.html">Albert Einstein</a>
    </footer>
</blockquote>
```

Het `<blockquote>` element wordt gebruikt om een **quote** of **citaat** te implementeren binnen een html document. Dit element kan een attribuut `cite` bevatten waarin de link wordt vermeld naar de persoon die dit citaat heeft geciteerd. Een blockquote kan een heading `<h1>` en paragrafen `<p>` bevatten. Daarnaast kunnen we in een `<footer>` vermelden wie dit citaat heeft bedacht. 

**CSS**

```
blockquote:first-of-type{
    font-size:28px;
    width:480px;  
    padding:14px;
}
blockquote:first-of-type footer{
    color:rgba(255,255,255,0.46);
    font-size:0.8em;
    margin-top:14px;
}
blockquote:first-of-type footer a{
    color:rgba(255,255,255,0.66);
}
```

Het css **pseudo element** `first-of-type` betekent in dit geval dat het eerste `<blockquote>` element zal opgemaakt worden.

**jQuery**

```
var q = $('blockquote:first-of-type');
q.attr('style','transform:rotate(90deg) translate(50%,50%);background:#009688;color:#fff;');
```

Via jQuery veranderen we de waarde van het `style` attribuut, namelijk: rotatie van het element 90 graden in uurwijzerzin, translatie van 50% in de X- en 50% in de Y richting, achtergrondkleur en tekstkleur.

![Quote from Albert Einstein](https://lh4.googleusercontent.com/-8E6kDhSti3Q/VPBObIPXMtI/AAAAAAAABP4/OJCuuDk9VAc/s0/js_quote.PNG "js_quote.PNG")

##50 Shades of grey

Generatie van 50 grijstinten via HTML, CSS en jQuery. Dankzij een witte achtergrond kunnen we deze grijstinten genereren door de **lightness** aan te passen in de CSS `hsla(hue, saturation%, lightness%, alpha)` functie. In dit voorbeeld varieert de **lightness** van `2%` tot en met `100%`.

**HTML**

```
<div id="shades"></div>
```

Dit `<div>` element fungeert als container voor toekomstige boxen. Dit element moet aanspreekbaar zijn via een `id`, `class` of `data`-attribuut.

**CSS**

```
.box{
    background:hsla(0,0%,0%,1);
    width:32px;
    height:32px;
    float:left;
    display:block;
    box-sizing:border-box;
    border:1px solid #000;
}
```

In de gekoppelde **CSS** definiëren we het begrip `.box`. Een `.box` bevat een vaste breedte en hoogte, een zwarte achtergrond, een zwarte border die geïntegreerd wordt tot de visuele breedte van een box via de CSS-eigenschap+waarde `box-sizing:border-box`. Uiteraard maken we deze boxen _links-zwevend_.

**jQuery**

```
var shades = $('#shades');
var result = '';
for(var i=1;i<=50;i++){
    result += '<div class="box" style="background-color:hsla(0,0%,' + (2*i) + '%,1)"></div>';
}
$(shades).html(result);
```

_Werkwijze:_

* Haal eerst het object op waarbinnen we de 50 shades of grey boxen willen genereren, dit doen we via jQuery: `$('#shades)`. `#shades` komt overeen met de `id` van de `<div>` waarbinnen we alles willen genereren.
* De generatie van de boxen voeren we uit via een `for-lus` en een tijdelijke variabele `result` die wek uitbreiden bij iedere iteratie van de aangemaakte `for-lus`.
* De `for-lus` bestaat uit een iterator startend vanaf 1 tot en met 50 en de laatste parameter vertellen we wat er moet gebeuren met de iterator. In dit geval tellen we bij iedere iteratie of lus 1 bij de vorige waarde van de iterator. Op deze manier kunnen we de **lightness** variëren.  
* `+=` betekent dat we bij de voorgaande waarde van een `string`, een extra waarde toevoegen ([concatenating](http://www.thefreedictionary.com/concatenating)).
* De resulterende `string` voegen we toe als inhoud aan het `<div>` element met de id `shades` via de jQuery methode of functie: `$(obj).html(result)`. 

**Resultaat**

![50 Shades of Grey](https://lh4.googleusercontent.com/-MER_OUSYEsU/VO4ge5Fyb5I/AAAAAAAABOk/lr8LdqpI3_g/s0/50shadesofgrey.PNG "50shadesofgrey.PNG")

##Color tones with Hue

* [HSL Color Picker](http://hslpicker.com/#d5eb14)

**HTML**

```
<div id="shades"></div>
```

**CSS**

```
.box{
    display:block;
    width:100%;
    height:36px;
    box-sizing:border-box;
    background:#000;
    line-height:36px;
    padding-left:18px;
    padding-right:18px;
}
```

**jQuery**

```
var shades = $('#shades');
var result = '', hue;
for(var i=0;i<=30;i++){
    hue = 360-i*12;
    result += '<div class="box" style="background-color:hsla(' + hue + ',84%,50%,1)">Hue: ' + hue + '</div>';
}
$(shades).html(result);
```

![Color tones with Hue](https://lh6.googleusercontent.com/-nHKBP49Cpvc/VPCNiMt9FTI/AAAAAAAABQQ/PoxE2R2To0c/s0/js_colortones.PNG "js_colortones.PNG")