Opdrachten
=========

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|
|Opleidingsonderdeel|E-design|

***
[TOC]
***

Briefing Opdracht 1: Wordpress Creativity Blog
-----------------------------------------------------------------

|Info||
|----|--|
|Werktitel|Wordpress|
|Subtitel|Wordspress Creativity Blog|
|Briefing|**25-02-2015** Briefing Opdracht 1|
|Milestone 1|**25-03-2015** Show your Blog|
|Opleveren|**20-05-2015** Opleveren opdracht 1: Wordpress|
|Groepswerk|Individueel|

Volg de tutorial: https://bitbucket.org/drdynscript/edesign/src/f62c1b2f817f751ee174a5ef7a80d0563bb3826f/cms.md?at=master om jouw WordPress website op te zetten via gratis Hosting (ByetHost).

###Verplichte content

* Minimaal **2x per week een post** of nieuwsbericht schrijven omtrent inspiratie, creativiteit, schetsen, designs, ... .
* Dit betekent ongeveer minimaal **18 posts tot de deadline**
* Een post bevat: **thumbnail, title, tags, categories, synopsis, body text incl. beelden en/of video's**, ...
* WordPress Theme & Templates (eigen of custom theme)

###Opleveren

- Werkende Wordpress Webapplicatie
	- URL: **{ahs loginnaam}blog.byethost{nr}.com**, bv.: phildpblog.byethost33.com
- Chamilo (zip bestand - Naam zip bestand: **edesign_201415_wordpress_2CMO_{ahs loginnaam}_.zip**)
    - Wordpress Thema en templates
    - Database dump

###Quotering

* 40% Design, 60% Implementatie

Briefing Opdracht 2: Resumé One Page Application
------------------------------------------------------------------------

|Info||
|----|--|
|Werktitel|ROPA|
|Subtitel|Resumé One Page Application|
|Briefing|**04-03-2015** Briefing Opdracht 2|
|Opleveren|**22-04-2015** Opleveren opdracht 2: ROPA|
|Groepswerk|Individueel|

###Persona's

- Gebruiker (User)

###Verplichte features

* __home__  
full background image of video, ghost buttons, lijniconen en navigatie  
de navigatie blijft op alle bookmarks zichtbaar, ook jouw personal brand of logo  
`id="home"`
* __about me__  
informatie over jezelf  
download cv knop, persoonlijke social media links (bv.: Twitter, LinkedIn, Dribbble, Behance, Pinterest **Nooit Facebook!**)  
`id="aboutme"`
* __skills__  
infografieken  
`id="skills"`
* __experiences__  
werkervaringen en studies  
`id="experiences"`
* __portfolio__  
minimaal 9 werken visualiseren  
later gelinkt naar jullie wordpress website  
`id="portfolio"`
* __blog__ (optioneel)  
minimaal laatste 3 nieuwsberichten visualiseren (niet het volledig bericht, enkel: titel, synopsis, datum, thumbnail)  
later gelinkt naar jullie wordpress website  
`id="blog"`
* __contact__  
Algemene contactgegevens (e-mail, gsm, nearby location, social media)  
Google Maps integratie (custom style)
`id="contact"`
* __footer__  
Copyright, Disclaimer Link, BTW nummer eventueel social media, Link naar aboutme en contact, ...

###Technische specificaties

####Technologieën & Bibliotheken

* HTML5
* CSS3
* JavaScript
* jQuery
* Bootstrap 3+
* Waypoints.js
* Animation.css
* ... (toelating vragen aan de docenten)

####Structuur applicatie

* ropa (folder)
	* content (folder)
		* documents (folder)
			* Bevat persoonlijke **cv.pdf**
		* icons (folder)
			* Bevat **favicon, touch icons en tile icons**
		* images (folder)
		* videos (folder)
	* data (folder)
	* scripts (folder)
		* vendor (folder)
			* **bootstrap.min.js**
			* **bootstrap.js**
			* **jquery.min.js**
			* **jquery.js**
			* **...**
		* **app.js**
	* styles (folder)
		* fonts (folder)
		* images (folder)
		* vendor (folder)
			* **bootstrap.min.css**
			* **bootstrap.css**
			* **...**
		* **app.css**			
	* **index.html**

####Structuur dossier

* Voorblad
* Inhoudsopgave
* Ideeënborden
* Moodboard
* Sitemap
* Wireframes
* Style Tiles
* Visual designs
* Implementatie
	* Gebruikte technologieën en bibliotheken
	* Mappenstructuur
	* Screenshots
	* Code snippets

###Opleveren

- Werkende One Page Application
	- URL: **{ahs loginnaam}ropa.byethost{nr}.com**, bv.: phildpropa.byethost33.com
- Chamilo (zip bestand - Naam zip bestand: **edesign_201415_ropa_2CMO_{ahs loginnaam}.zip**)
    - resources (folder)
	    - dossier.pdf
    - ropa (folder)
	    - zie structuur van de app
- Afgedrukt
    - dossier.pdf

###Quotering

* 60% Design, 40% Implementatie

Briefing Opdracht 3: Campagne
-----------------------------------------------------------------------

|Info||
|----|--|
|Werktitel|Campagne|
|Subtitel|Campagne ...|
|Briefing|**01-04-2015** Briefing Opdracht 4|
|Opleveren|**20-05-2015** Opleveren opdracht 4: Campagne|
|Groepswerk|Groep bestaande uit max. 4 studenten|
