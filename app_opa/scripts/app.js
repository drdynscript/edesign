(function($){

    /* Scroll or Go Down to the first wrapper */
    $('.godown').on('click', function(e){

        goToFirstPage();

    });

    /* Read More -> Go Down to the first wrapper */
    $('.btn-readmore').on('click', function(e){

        goToFirstPage();

    });

    function goToFirstPage(){
        var obj = $('#about');
        if(obj.length > 0){
            $('html, body').animate({
                scrollTop: obj.offset().top
            }, 1000, 'swing', function(){
            });
        }
    }

    /* Scroll to top */
    $('.gototop').on('click', function(e){
        e.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 1000, 'swing', function(){
        });

        return false;
    });

    /* Listen to scrolling event */
    $(window).scroll(function(){
        if($(this).scrollTop() > 100){
            $(".gototop").fadeIn('slow');
            $(".godown").fadeOut('slow');
        }else{
            $(".gototop").fadeOut('slow');
            $(".godown").fadeIn('slow');
        }
    });

    /* List to menu item click */
    $('.nav>li>a').each(function(i){

        $( this ).click(function(ev){
            ev.preventDefault();

            $('html, body').animate({
                scrollTop: $($( this).attr('href')).offset().top
            }, 1000, 'swing', function(){
            });

            return false;
        });
    });

    /*
    Problems:
    * jQuery animation not working in combination with CSS transitions or animations
     */

    /* Waypoint: About */
    $('#about').waypoint(function(direction){
        if(direction == 'down'){
            $('.navbar').addClass('navbar-contentscrolled');
        } else {
            $('.navbar').removeClass('navbar-contentscrolled');
        }
    }, { offset: '65%' } );

})(jQuery);