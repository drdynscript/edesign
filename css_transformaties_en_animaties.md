CSS Transformaties
==================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

Transformaties laten toe om de positie, grootte en vorm van een element te veranderen. In CSS kunnen we een element: verplaatsen (move), schalen (scale), roteren (rotate) en uitrekken/scheeftrekken (skew). Zowel 2D als 3D transformaties kunnen toegepast worden in CSS. Opgelet: het transformatiepunt ligt standaard in het midden van het element!

Transformaties kunnen toegekend worden aan een element via de transform eigenschap. Voor de meeste CSS3 eigenschappen, moeten browser specifiek prefixen toegevoegd worden aan deze eigenschappen:

- Safari en Chrome: 	`-webkit-`
- Mozilla:			`-moz-`
- IE:			`-ms-`
- Opera:			`-o-`


2D-translatie
----------------

Translatie of verschuiving van een element, in dit geval in de 2D-ruimte. Translatie wordt in CSS gerealiseerd via de `translate` methoden:

- `translate(tx[,ty])`
Indien ty niet gespecifieerd wordt, zal ty = 0
- `translateX(tx)`
Translatie langs de X-as
- `translateY(ty)`
Translatie langs de Y-as

De waarden van translatie (tx, ty) kunnen uitgedrukt worden in een lengte (getal gevolgd door een lengte-eenheid, zoals: px, em, in, pt, mm, …) of in percentage (%).

```html
<div class="box orig">
  BOX
</div>
<div class="box eff1">
  BOX
</div>
```

```css
.box{
  display:block;
  background:#f60;
  width:160px;
  height:90px;
  text-align:center;
  line-height:90px;
  font-size:20px;
}
.orig{
  position:fixed;
  opacity:0.2;
}
.eff1{
  -webkit-transform:translate(20px,50%);
  -moz-transform:translate(20px,50%);
  -ms-transform:translate(20px,50%);
  -o-transform:translate(20px,50%);
  transform:translate(20px,50%);
}
```

In dit voorbeeld verplaatsen we een element: 20px verschuiving langs de X-as, 50% van de hoogte van het element verschuiving langs de Y-as.

> **Output in de browser:**
>
>  ![enter image description here](https://lh3.googleusercontent.com/-XhQh7vuBhZc/VBwDwnoUdrI/AAAAAAAAAN8/u-6OAzneuVg/s0/translate1.png "translate1.png")

2D-schalen
-------------

Elementen kunnen geschaald worden in de X- en/of Y-richting. Schalen wordt in CSS gerealiseerd via de `scale` methoden:

- `scale(sx[,sy])`
Indien sy niet gespecifieerd wordt, zal sy = 1
- `scaleX(sx)`
- `scaleY(sy)`

De waarden van schalen (sx, sy) moeten uitgedrukt worden met een getal (positief of negatief floating-point getal).

```css
…
.eff1{
 -webkit-transform:translate(100%, 100%) rotate(45deg);
  -moz-transform:translate(100%, 100%) rotate(45deg);
  -ms-transform:translate(100%, 100%) rotate(45deg);
  -o-transform:translate(100%, 100%) rotate(45deg);
  transform:translate(100%, 100%) rotate(45deg);
}
```

In het voorbeeld voeren we eerst een translatie uit van 100% in beide richtingen (x en y). Gevolgd door het roteren in uurwijzerzin met 45 graden (rotatiepunt ligt in het midden van het element)
Overeenkomstige matrix: [cos(angle) sin(angle) -sin(angle) cos(angle) 0 0].

> **Output in de browser:**
>
>  ![enter image description here](https://lh6.googleusercontent.com/-qNmnCdSZjY4/VBwFBVuEk1I/AAAAAAAAAOI/XhXo5TkOdD8/s0/scale1.PNG "scale1.PNG")

2D-scheeftrekken
---------------------

Elementen kunnen scheefgetrokken worden in de X- en/of Y-richting. Scheeftrekken wordt in CSS gerealiseerd via de `skew` methoden:

- `skew(ax[ay])`
Indien ay niet gespecifieerd wordt, zal ay = 0deg
- `skewX(ax)`
Scheeftrekken langs de X-as
- `skewY(ay)`
Scheeftrekken langs de Y-as

De waarden van scheeftrekken (ax, ay) wordt uitgedrukt in aantal graden: positief of negatief getal gevolgd door `deg`.

```css
…
.eff1{
 -webkit-transform:translate(100%, 100%) skew(45deg, -30deg);
  -moz-transform:translate(100%, 100%) skew(45deg, -30deg);
  -ms-transform:translate(100%, 100%) skew(45deg, -30deg);
  -o-transform:translate(100%, 100%) skew(45deg, -30deg);
  transform:translate(100%, 100%)  skew(45deg, -30deg);
}
```

In het voorbeeld voeren we eerst een translatie uit van 100% in beide richtingen (x en y). Gevolgd door het scheeftrekken langs de X-as met 45 graden en -30 graden langs de Y-as.

> **Output in de browser:**
>
>  ![enter image description here](https://lh4.googleusercontent.com/-bQ12SYxQGFg/VBwJ53GUOxI/AAAAAAAAAOg/iMlIQpRQ6BE/s0/skew.png "skew.png")

2D-matrix
------------

Transformaties kunnen ook voorzien worden via een 2D-matrix. Een matrix in CSS bevat 6 argumenten:

- `matrix(a, c, b, d, e, f)`

>
>
> ![enter image description here](https://lh5.googleusercontent.com/-mS7ODlQMU2E/VBwL6fJPV7I/AAAAAAAAAO0/pdf0aYHjsfA/s0/matrix.png "matrix.png")
>
> - a en d zorgen voor het schalen van een element
- c en b zorgen voor het roteren of schuintrekken van een element
- e en f zorgen voor de translatie in de X- en Y-richting
- Default matrix: a = 1, c = 0, b = 0, d = 1, e = 0 en f = 0
>
> ![enter image description here](https://lh6.googleusercontent.com/-pf7D7GLAOA4/VBwMdAWR8JI/AAAAAAAAAPA/xFvP9pyJOyk/s0/matrix2.png "matrix2.png")

Enkele voorbeelden:

- `transform:matrix(1,0,0,1,20,30);`
Translatie 20px in de X-richting en 30px in de Y-richting
- `transform:matrix(1,0.5,0.6,1,0,0);`
Schuintrekken van het element
- `transform:matrix(0,866,0.5,-0.5,0,866,0,0);`
Rotatie wijzerzin van het element (30 graden rotatie)

```css
…
.eff1{
 -webkit-transform:matrix(0.866,0.5,-0.5,0.866,100,100);
  -moz-transform:matrix(0.866,0.5,-0.5,0.866,100,100);
  -ms-transform:matrix(0.866,0.5,-0.5,0.866,100,100);
  -o-transform:matrix(0.866,0.5,-0.5,0.866,100,100);  transform:matrix(0.866,0.5,-0.5,0.866,100,100);
}
```

De matrix resulteert in een translatie van 100 pixels in de Y- en de X-richting, rotatie van het element met 30 graden in uurwijzerzin.


> **Output in de browser:**
>
>  ![enter image description here](https://lh6.googleusercontent.com/-QQ5AlCYZyq4/VBwMyOetRTI/AAAAAAAAAPM/WrLUqD6-EUU/s0/matrix3.png "matrix3.png")


2D-transformatiepunt
--------------------------

Bij “default” ligt dit punt in het midden van het element, de initiële waarde bedraagt 50% 50%. Roteren we bv. een element, dan ligt het rotatiepunt initieel in het midden van het element. Het transformatie punt kunnen we via CSS op een andere plaats leggen en dit via de ``transform-origin` eigenschap:

- `transform-origin(x [, y])`

De waarden van het transformatiepunt (x, y) kunnen uitgedrukt worden in een lengte (getal gevolgd door een lengte-eenheid, zoals: px, em, in, pt, mm, …) , in percentage (%) of via gedefinieerde keywords (left, center, right, top en bottom).


```css
…
.eff1{
 -webkit-transform-origin:0 0;
  -moz-transform-origin:0 0;
  -ms-transform-origin:0 0;
  -o-transform-origin:0 0;
  transform-origin:0 0;
  -webkit-transform:rotate(60deg);
  -moz-transform:rotate(60deg);
  -ms-transform:rotate(60deg);
  -o-transform:rotate(60deg);
  transform:rotate(60deg);
}
```

In dit voorbeeld verplaatsen we het transformatiepunt naar de linkerbovenhoek van het element via de waarden `0 0` of `left top`. Vervolgens roteren we het element met 60 graden in uurwijzerzin.

> **Output in de browser:**
>
>  ![enter image description here](https://lh6.googleusercontent.com/-XQ_bNlIDH6g/VBwNqdDgNII/AAAAAAAAAPc/2EM_7v4TReE/s0/transformatiepunt.png "transformatiepunt.png")

2D-voorbeelden
-------------------

###Voorbeeld 1: translatie

```css
#example1 .box:nth-of-type(1){
-ms-transform:translate(220px, 160px);
-webkit-transform:translate(220px, 160px);
transform:translate(220px, 160px);
}
#example1 .box:nth-of-type(2){
-ms-transform:translate(50%, 50%);
-webkit-transform:translate(50%, 50%);
transform:translate(50%, 50%);
}
#example1 .box:nth-of-type(3){
-ms-transform:translate(0, -50px);
-webkit-transform:translate(0, -50px);
transform:translate(0, -50px);
}
```

> **Output:**
> ![enter image description here](https://lh3.googleusercontent.com/-e1UvaCjKh9E/VBwRUjOYeKI/AAAAAAAAAP4/D2R0TLI4Ph4/s0/ex1.PNG "ex1.PNG")

###Voorbeeld 2: rotatie

```css
#example2 .box:nth-of-type(1){
-ms-transform:rotate(60deg);
-webkit-transform:rotate(60deg);
transform:rotate(60deg);
}
#example2 .box:nth-of-type(2){
-ms-transform:rotate(-36deg);
-webkit-transform:rotate(-36deg);
transform:rotate(-36deg);
}
```

> **Output:**
> ![enter image description here](https://lh4.googleusercontent.com/-I3_s3anQVxk/VBwRajtRf0I/AAAAAAAAAQE/h3fGf2Ytc6s/s0/ex2.PNG "ex2.PNG")

###Voorbeeld 3: schalen

```css
#example3 .box:nth-of-type(1){
-ms-transform:scale(2.6);
-webkit-transform:scale(2.6);
transform:scale(2.6);
}
#example3 .box:nth-of-type(2){
-ms-transform:scale(2, 1.5);
-webkit-transform:scale(2, 1.5);
transform:scale(2, 1.5);
}
#example3 .box:nth-of-type(3){
-ms-transform:scaleY(-1);
-webkit-transform:scaleY(-1);
transform:scaleY(-1);
}
```

> **Output:**
> ![enter image description here](https://lh3.googleusercontent.com/-jclSH0UimFE/VBwRpRBdM4I/AAAAAAAAAQQ/oJI5oK8q650/s0/ex3.PNG "ex3.PNG")

###Voorbeeld 4: scheeftrekken

```css
#example4 .box:nth-of-type(1){
-ms-transform:skew(30deg);
-webkit-transform:skew(30deg);
transform:skew(30deg);
}
#example4 .box:nth-of-type(2){
-ms-transform:skew(60deg, 20deg);
-webkit-transform:skew(60deg, 20deg);
transform:skew(60deg, 20deg);
}
#example4 .box:nth-of-type(3){
-ms-transform:skewY(-30deg);
-webkit-transform:skewY(-30deg);
transform:skewY(-30deg);
}
```

> **Output:**
> ![enter image description here](https://lh3.googleusercontent.com/-hSg2oQBMrKQ/VBwRtq682pI/AAAAAAAAAQg/cFE6_7UHBMM/s0/ex4.PNG "ex4.PNG")

###Voorbeeld 5: transformatiepunt met rotatie

```css
#example6 .box:nth-of-type(1){
-ms-transform:rotate(30deg);
-webkit-transform:rotate(30deg);
transform:rotate(30deg);
-ms-transform-origin:left top;
-webkit-transform-origin:left top;
transform-origin:left top;
}
#example6 .box:nth-of-type(2){
-ms-transform:rotate(30deg);
-webkit-transform:rotate(30deg);
transform:rotate(30deg);
-ms-transform-origin:60% 40%;
-webkit-transform-origin:60% 40%;
transform-origin:60% 40%;
}
#example6 .box:nth-of-type(3){
-ms-transform:rotate(30deg);
-webkit-transform:rotate(30deg);
transform:rotate(30deg);
-ms-transform-origin:80% bottom;
-webkit-transform-origin:80% bottom;
transform-origin:80% bottom;
}
```

> **Output:**
> ![enter image description here](https://lh4.googleusercontent.com/-j-Y8D_9xeio/VBwRxTY-c5I/AAAAAAAAAQs/jmcUY2op2rk/s0/ex5.PNG "ex5.PNG")

###Voorbeeld 6: combinatie van transformatiemethoden

```css
#example7 .box:nth-of-type(1){
-ms-transform:translate(100%, 100%) rotate(30deg) skewX(15deg) scale(2);
-webkit-transform:translate(100%, 100%) rotate(30deg) skewX(15deg) scale(2);
transform:translate(100%, 100%) rotate(30deg) skewX(15deg) scale(2);;
-ms-transform-origin:50% 50%;
-webkit-transform-origin:50% 50%;
transform-origin:50% 50%;
}
```

> **Output:**
> ![enter image description here](https://lh5.googleusercontent.com/-muUBLdiSixA/VBwR1XP7mFI/AAAAAAAAAQ4/_qa_PoOZXq4/s0/ex6.PNG "ex6.PNG")

In CSS kunnen we op twee manieren elementen animeren, namelijk via de `transition` methode en via de `animation` methode. Bij de `transition` is een actie noodzakelijk door de gebruiker om de animatie uit te voeren. Bij de `animation` methode is dit niet . Enkel de `animation` methode maakt gebruik van **keyframes**.

CSS Transition
============

Transitie via CSS laat toe om de transitie tussen twee statussen van een element te definiëren. Deze statussen kunnen gedefinieerd worden via bepaalde pseudo-klassen, zoals: `:hover` en `active` of via JavaScript (door het toevoegen of verwijderen van `class`-attribuut waarden.

De `transition` eigenschap is een verkorte eigenschap voor de lijst van eigenschappen:  `transition-property`, `transition-duration`, `transition-timing-function`, en `transition-delay`.

Transition eigenschappen
------------------------------------

De `transition-property` eigenschap wordt gebruikt om de CSS-eigenschappen te definiëren waarop de transitie zal toegepast worden, bv.: all (op alle CSS-eigenschappen), top, left, background, color, ... .

De `transition-duration` eigenschap wordt gebruikt om de duur van de transitie te specificeren. De default-waarde bedraagt `0s`. De waarden kunnen uitgedrukt worden in seconden of milliseconden.

De `transition-timing-function` eigenschap wordt gebruikt om te beschrijven hoe de tussenliggende waarden van de CSS-eigenschappen beïnvloed worden door het transitie-effect. De snelheid van de transitie varieert over de duur van deze transitie door de acceleratie-curve, gekend als **easing-functies**.

Veel gebruikte timing-functies zijn `linear`, `ease`, `ease-in`, `ease-out`, `ease-in-out`, `step-start` en `step-end`.

|Timing-function|Curve|Description|
|---------------|-----|-----------|
|`linear`|![enter image description here](https://lh3.googleusercontent.com/-cRZFQb5Rmbs/VBw7HWfxTnI/AAAAAAAAAR0/mwvhKlCwKA8/s0/cubic-bezier,linear.png "cubic-bezier,linear.png")|Constante snelheid: `cubic-bezier(0.0, 0.0, 1.0, 1.0)`|
|`ease`|![enter image description here](https://lh4.googleusercontent.com/-f_1Tl1DKnKA/VBw7NmRpZSI/AAAAAAAAASA/hV7DEo-9duc/s0/cubic-bezier,ease.png "cubic-bezier,ease.png")|Gelijkaardig met `ease-in-out`. De acceleratie in het begin is sneller en vanaf het midden treedt vertraging op:  `cubic-bezier(0.25, 0.1, 0.25, 1.0)`|
|`ease-in`|![enter image description here](https://lh5.googleusercontent.com/-PlJzXZ8IEFc/VBw7TEikImI/AAAAAAAAASM/_uIt_xGhp18/s0/cubic-bezier,ease-in.png "cubic-bezier,ease-in.png")|De animatie begint traag waarna de acceleratie progressief toeneemt: `cubic-bezier(0.42, 0.0, 1.0, 1.0)`|
|`ease-out`|![enter image description here](https://lh5.googleusercontent.com/-LqHUcNhapYI/VBw7ao3GbMI/AAAAAAAAASY/MrtLXN6eoFU/s0/cubic-bezer,ease-out.png "cubic-bezer,ease-out.png")|De annimatie start snel waarna de acceleratie progressief afneemt: `(0.0, 0.0, 0.58, 1.0)`|
|`ease-in-out`|![enter image description here](https://lh4.googleusercontent.com/-l9cWvXJUsp4/VBw7fchyyII/AAAAAAAAASk/43HqEQIc7HU/s0/cubic-bezier,ease-in-out.png "cubic-bezier,ease-in-out.png")|De animatie start traag, accelereert dan, waarna het terug afneemt: `0.42, 0.0, 0.58, 1.0)`|
|`step-start`|![enter image description here](https://lh6.googleusercontent.com/-ByeenXxNOuY/VBw7mYTwf5I/AAAAAAAAASw/2KJMC-VOJl4/s0/steps%281,start%29.png "steps&#40;1,start&#41;.png")|De animatie springt direct naar de eindstatus: `steps(1, start)`|
|`step-end`|![enter image description here](https://lh3.googleusercontent.com/-ANu_sGKYv60/VBw7wnWgK5I/AAAAAAAAAS8/JLC8ExtsDQw/s0/steps%281,end%29.png "steps&#40;1,end&#41;.png")|De animatie blijft in de initiële status en springt pas op het einde naar zijn eindstatus: `steps(1, end)`|

Naast deze gekende timing functies, zijn er nog twee functies toepasbaar, namelijk `cubic-bezier` en `steps`.

De `cubic-bezier` functie definieert een **cubic bézier curve**. Deze curves worden vaak gebruikt om de snelheid in het begin en op het einde van de animatie langzaam toe- en af te laten nemen.

|Timing-function|Curve|Description|
|---------------|-----|-----------|
|`cubic-bezier`|![enter image description here](https://lh5.googleusercontent.com/-F2KMVdtndp0/VBxVn-rdEPI/AAAAAAAAAUA/FOcN2ZwFy_Y/s0/cubic-bezier,+example+%281%29.png "cubic-bezier, example &#40;1&#41;.png")|P0 (0,0) Initiële status. P3 (1,1) Finale status. P1 en P2 hebben meestal een waarde tussen 0 en 1. Ligt deze waarde daarbuiten, dan zullen we waarschijnlijk een **bouncing-effect** realiseren.|

```
cubic-bezier(x1, y1, x2, y2)
```

- x1 en x2 moeten een waarde hebben tussen 0 en 1
- y1 en y2 kunnen zowel postieve- als negatieve waarden bevatten

```
Enkele voorbeelden:

cubic-bezier(0.1, 0.7, 1.0, 0.1)
cubic-bezier(0, 0, 1, 1)
cubic-bezier(0.1, -0.6, 0.2, 0)
cubic-bezier(0, 1.1, 0.8, 4)
```

De `steps` functie definieert het aantal stappen dat binnen de timing functie uitgevoerd zullen worden. Deze functie is beter gekend als een **staircase** functie. De syntax: `steps(number_of_steps, direction)`. De `number_of_steps` is een positieve integer (1 of hoger), de `direction` is `start` of `end`. Dit laatste argument is facultatief en heeft `end` als standaard waarde.

|Direction: start|Direction: end|
|----------------|--------------|
|`steps(2, start)`|`steps(4, end)`|
|![enter image description here](https://lh6.googleusercontent.com/-K0GSlOfsX5A/VBxatqp3VlI/AAAAAAAAAUU/WkHU0V-bb58/s0/steps%282,start%29.png "steps&#40;2,start&#41;.png")|![enter image description here](https://lh3.googleusercontent.com/-AUqTseu1IKY/VBxayDDiG2I/AAAAAAAAAUg/UuBfX0C-dS8/s0/steps%284,end%29.png "steps&#40;4,end&#41;.png")|

De `transition-delay` eigenschap wordt gebruikt om de transitie van een CSS-eigenschap een bepaalde duur uit te stellen. De default-waarde bedraagt `0s`. De waarden kunnen uitgedrukt worden in seconden of milliseconden.

Voorbeelden
--------------------------------

###CSS instellingen voor voorbeeld 1 tot en met 4

```css
.example{
    background:rgba(33,33,33,1);
    min-height:286px;
    overflow:hidden;
    position:relative;
    margin-bottom:48px;
}
.box{
    position:absolute;
    top:0;
    left:0;
    background:rgba(221,211,211,0.86);
    height:80px;
    width:25%;
    line-height:80px;
    text-align: center;
}
.example .box:nth-of-type(1){
    background:rgba(0,170,204,0.86);
}
.example .box:nth-of-type(2){
    background:rgba(204,0,119,0.86);
}
.example .box:nth-of-type(3){
    background:rgba(187,204,0,0.86);
}
.example .box:nth-of-type(4){
    background:rgba(238,153,0,0.86);
}
.example .box:nth-of-type(5){
    background:rgba(211,211,211,0.86);
}
```

###Voorbeeld 1: transitie van rotatie en translatie

```css
#example1 .box:nth-of-type(1){
    transform-origin:left top;
    transition:all 2s ease 0s;
}
#example1:hover .box:nth-of-type(1){
    transform:rotate(360deg);
    transform-origin:left top;
}
#example1 .box:nth-of-type(2){
    transition:all 4s ease 0s;
}
#example1:hover .box:nth-of-type(2){
    transform:rotate(3600deg) scale(2,2) translate(100%, 100%);
}
#example1 .box:nth-of-type(3){
    transform:rotate(150deg);
    transform-origin:-100px -100px;
}
```

> **Output:**
> ![Transitie van rotatie en translatie](https://lh6.googleusercontent.com/-fyGVJVWZmy4/VPgmdfN0i1I/AAAAAAAABTQ/sSXuJR1NGTA/s0/transition1.PNG "transition1.PNG")

###Voorbeeld 2: transition-delay

```css
#example2 .box{
    width:25%;
    height:25%;
    transition:all 3s ease-in 0s;
}
#example2 .box:nth-of-type(1){
    top:0;
    transition-delay:0s;
}
#example2 .box:nth-of-type(2){
    top:25%;
    transition-delay:1s;
}
#example2 .box:nth-of-type(3){
    top:50%;
    transition-delay:1.5s;
}
#example2 .box:nth-of-type(4){
    top:75%;
    transition-delay:2s;
}
#example2:hover .box{
    left:75%;
    transform:rotate(360deg);
}
```

> **Output:**
> ![Transition Delay](https://lh4.googleusercontent.com/-zZ9mFpwz94M/VPgmp8goA7I/AAAAAAAABTc/AB8JWSsvWsE/s0/transition2.PNG "transition2.PNG")


###Voorbeeld 3: sequentie van transities

```css
#example3 .box{
    width:10%;
    height:25%;
    transition-property:left, top;
    transition-timing-function:ease;
    transition-duration:2s,1s;
    transition-delay:0s,2s;
    top:0;
    left:0;
}
#example3 .box:nth-of-type(1){
    left:30%;
}
#example3 .box:nth-of-type(2){
    left:20%;
    transition-delay:0.2s,2.2s;
}
#example3 .box:nth-of-type(3){
    left:10%;
    transition-delay:0.4s,2.4s;
}
#example3 .box:nth-of-type(4){
    left:0%;
    transition-delay:0.6s,2.6s;
}
#example3:hover .box{
    left:90%;
    top:75%;
}
```

> **Output:**
> ![Sequentie van transities](https://lh6.googleusercontent.com/-blxVXAxQ6dM/VPgm0ABdoEI/AAAAAAAABTo/eT5y37Ihu3Y/s0/transition3.PNG "transition3.PNG")

###Voorbeeld 4: Easing functies transities

```css
#example4 .box{
    width:10%;
    height:20%;
    transition-property:left;
    transition-timing-function:linear;
    transition-duration:2s;
    transition-delay:0s;
    top:0;
    left:0;
}
#example4 .box:nth-of-type(1){
    top:0;
    transition-timing-function:linear;
}
#example4 .box:nth-of-type(2){
    top:20%;
    transition-timing-function:ease;
}
#example4 .box:nth-of-type(3){
    top:40%;
    transition-delay:0.4s,2.4s;
    transition-timing-function:ease-in;
}
#example4 .box:nth-of-type(4){
    top:60%;
    transition-delay:0.6s,2.6s;
    transition-timing-function:ease-out;
}
#example4 .box:nth-of-type(5){
    top:80%;
    transition-delay:0.6s,2.6s;
    transition-timing-function:ease-in-out;
}
#example4:hover .box{
    left:90%;
}
```

> **Output:**
> ![Easing functies transities](https://lh4.googleusercontent.com/-0N-r7YNRQsI/VPgnCt8pWbI/AAAAAAAABT0/CW3hHTJrT-w/s0/transition4.PNG "transition4.PNG")

###Voorbeeld 5: Off Canvas Menu

```css
#example5 *{
    box-sizing: border-box;
}
#example5 #navigation-main{
    position:absolute;
    top:0;
    left:-100%;
    width:300px;
    height:100%;
    background:rgba(0,170,204,0.86);
    transition:left 800ms ease 0s;
}
#example5 #navigation-toggle{
    position:absolute;
    top:0;
    right:0;
    z-index:2;
    background:rgba(170,170,170,1);
    font-size:2em;
    font-weight:800;
    text-align:center;
    padding:6px 12px;
}
#example5 #navigation-toggle>i{
    transition:all 600ms ease 0s;
}
#example5 #navigation-toggle.open>i{
    transform:rotate(90deg);
}
#example5 #navigation-main ul{
    margin:0;
    padding:0;
    width:100%;
    height:100%;
    list-style: none;
}
#example5 #navigation-main ul>li{
    height:20%;
    border-bottom:1px solid rgba(33,33,33,0.86);
}
#example5 #navigation-main ul>li>a{
    display:block;
    text-decoration: none;
    height:100%;
    width:100%;
    padding:18px 24px;
    color:rgba(33,33,33,1);
    font-weight:800;
}
#example5 #navigation-main ul>li:last-of-type{
    border-bottom-width:0;
}
#example5 #navigation-main ul>li>a:hover{
    background:rgba(187,204,0,0.86);
}
#example5 #navigation-main.open{
    left:0%;
    transition:left 800ms ease 0s;
}
```

> **Output:**
> ![Off Canvas Menu](https://lh6.googleusercontent.com/-NSGytX2lAUo/VPgnOowP7_I/AAAAAAAABUA/uASWLdJGix4/s0/transition5.PNG "transition5.PNG")


CSS Animation
============

Nog verder in te vullen!

Voorbeelden
------------------

###Voorbeeld 1

```css
#example1 .box:nth-of-type(1){
    -webkit-animation-duration: 3s;
    -webkit-animation-name:slidein;
    -webkit-animation-delay:0s;
    -webkit-animation-direction:normal;/*normal, reverse, alternate, alternate-reverse, (normal, reverse, alternate)*/
    -webkit-animation-iteration-count:1;/*1, 2.3, infinite, (3, 0.5, infinite)*/
    -webkit-animation-play-state:running;/*paused, running, (running, paused)*/
    -webkit-animation-timing-function:ease;
}
@-webkit-keyframes slidein{
    from{
        left:-100%;
    }

    to{
        left:0;
    }
}
```

###Voorbeeld 2

```css
#example2 .box:nth-of-type(1){
    -webkit-animation-duration: 3s;
    -webkit-animation-name:snakerun;
    -webkit-animation-delay:0s;
    -webkit-animation-direction:alternate;/*normal, reverse, alternate, alternate-reverse, (normal, reverse, alternate)*/
    -webkit-animation-iteration-count:infinite;/*1, 2.3, infinite, (3, 0.5, infinite)*/
    -webkit-animation-play-state:running;/*paused, running, (running, paused)*/
    -webkit-animation-timing-function:ease;
}
@-webkit-keyframes snakerun{
    from{
        left:0;
        top:0;
    }
    25%{
        left:75%;
        top:0;
    }
    50%{
        top:90%;
        left:75%;
    }
    75%{
        left:0;
        top:90%;
    }
    100%{
        top:0;
        left:0;
    }
}
```

###Voorbeeld 3

```css
#example3 .box{
    width:6%;
    height:30%;
    background:url('../images/ike.png') no-repeat;
    background-size:cover;
    text-indent: -999em;
}
#example3 .box:nth-of-type(1){
    -webkit-animation-duration: 3s;
    -webkit-animation-name:pong;
    -webkit-animation-delay:0s;
    -webkit-animation-direction:normal;/*normal, reverse, alternate, alternate-reverse, (normal, reverse, alternate)*/
    -webkit-animation-iteration-count:infinite;/*1, 2.3, infinite, (3, 0.5, infinite)*/
    -webkit-animation-play-state:running;/*paused, running, (running, paused)*/
    -webkit-animation-timing-function:ease;
}
@-webkit-keyframes pong{
    from{
        left:0;
        top:35%;
        transform:rotate(0deg);
    }
    25%{
        left:47%;
        top:75%;
        transform:rotate(360deg);
    }
    50%{
        left:94%;
        top:35%;
        transform:rotate(360deg);
    }
    75%{
        left:47%;
        top:0;
        transform:rotate(-360deg);
    }
    100%{
        top:35%;
        left:0;
        transform:rotate(-360deg);
    }
}
```

###Voorbeeld 4

**CSS** 

```css
#example4 .speaker{
    width:64px;
    height:64px;
    background:url('../images/speaker3.png') no-repeat;
    background-size:cover;
    text-indent: -999em;
}
#example4 .speaker:nth-of-type(1){
    -webkit-animation-duration: 840ms;
    -webkit-animation-name:speaker;
    -webkit-animation-delay:0s;
    -webkit-animation-direction:normal;/*normal, reverse, alternate, alternate-reverse, (normal, reverse, alternate)*/
    -webkit-animation-iteration-count:infinite;/*1, 2.3, infinite, (3, 0.5, infinite)*/
    -webkit-animation-play-state:running;/*paused, running, (running, paused)*/
    -webkit-animation-timing-function:ease;
}
@-webkit-keyframes speaker{
    0% {transform:scale(20);}
    5% {transform:scale(18);}
    10% {transform:scale(14);}
    15% {transform:scale(8);}
    20% {transform:scale(4);}
    25% {transform:scale(2);}
    30% {transform:scale(14);}
    35% {transform:scale(18);}
    40% {transform:scale(20);}
    45% {transform:scale(32);}
    50% {transform:scale(24);}
    55% {transform:scale(28);}
    60% {transform:scale(32);}
    65% {transform:scale(8);}
    70% {transform:scale(2);}
    75% {transform:scale(1);}
    80% {transform:scale(3);}
    85% {transform:scale(4);}
    90% {transform:scale(16);}
    95% {transform:scale(32);}
    100% {transform:scale(20);}
}
```



CSS Animation Bibliotheken
=======================

> **Animation for all elements**
>
* [Bounce.js](http://www.bouncejs.com)
* [Animate.css](http://daneden.github.io/animate.css/)
* [Spinkit](http://tobiasahlin.com/spinkit/)
* [Magic Animations](http://www.minimamente.com/example/magic_animations/)
* [CSShake](http://elrumordelaluz.github.io/csshake/#1)
* [Animatable](http://leaverou.github.io/animatable/)

---

> **Animation for Buttons**
>
* [Button Builder](http://unicorn-ui.com/buttons/builder/)
* [beautons](http://csswizardry.com/beautons/)

---

> **Animation for spinners**
>
* [CSS Spinners](http://projects.lukehaas.me/css-loaders/)
*

---

> **Various**
>
* [ODOMETER](http://github.hubspot.com/odometer/docs/welcome/)
* [hover.css](http://ianlunn.github.io/Hover/)
* [Effeckt.css](https://github.com/h5bp/Effeckt.css)
* [TRIDIC](http://tridiv.com/)

Voorbeelden
------------------

###Voorbeeld1: Animate.css 'swing'

**HTML**

```
<a href="#" class="btnAnimate">Animate</a>
<img class="box animated" src="http://lorempixel.com/400/200/sports/1" />
```

Dit `<div>` element fungeert als container voor toekomstige boxen. Dit element moet aanspreekbaar zijn via een `id`, `class` of `data`-attribuut.

**CSS**

```
.box {
    box-sizing:border-box;
}
.btnAnimate{
    border:2px solid rgba(33,33,33,1);
    color:rgba(33,33,33,1);
    padding:15px 30px;
    text-transform:uppercase;
    border-radius:10px;
    text-decoration:none;
    display:inline-block;
    margin-bottom:15px;
}
.btnAnimate:hover{
    background:rgba(33,33,33,1);
    color:rgba(255,255,255,1);
}
```

**jQuery**

```
var animationEffect = 'swing';

$('.btnAnimate').on('click',function(ev){
    if(!$('.box').hasClass(animationEffect)){
        $('.box').addClass(animationEffect);
        $('.box').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(ev){
            $(this).removeClass(animationEffect);
        });
    }
});
```

> **Links**
>
* [Animate.css](http://daneden.github.io/animate.css/)
* [Animate.css GitHub](https://github.com/daneden/animate.css)

Bibliografie
=========

> **2D-transformaties**
>
> - <http://www.w3schools.com/css/css3_2dtransforms.asp>
- <https://developer.mozilla.org/en-US/docs/Web/CSS/transform>
- <https://developer.mozilla.org/en-US/docs/Web/CSS/translation-value>
- <http://dev.opera.com/articles/understanding-the-css-transforms-matrix/>
- <http://www.useragentman.com/blog/2011/01/07/css3-matrix-transform-for-the-mathematically-challenged/>
- <http://www.w3schools.com/cssref/css3_pr_transform-origin.asp>
- <https://developer.mozilla.org/en-US/docs/Web/CSS/transform-origin>
- <https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function>
>
> **3D-transformaties:**
>
> - <http://desandro.github.io/3dtransforms/docs/perspective.html>
>   
> **2D-transities**
>
> - <https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Using_CSS_transitions>
- <http://css3.bradshawenterprises.com/transitions/>
